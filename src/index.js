const ipldata = require('./ipl');
const csv = require('csvtojson');
var fs = require("fs");

csv().fromFile('../data/matches.csv').then((jsonObj) => {
    let matchId2016 = ipldata.matchIdForYear(jsonObj, '2016');
    let matchId2015 = ipldata.matchIdForYear(jsonObj, '2015');
    let noOfMatchesPlayed = ipldata.getNoOfMatchesPlayed(jsonObj);
    console.log('Matches played per year');
    console.log(noOfMatchesPlayed);
    let noOfMatchesWon = ipldata.getNoOfMatchesWonPerTeamPerYear(jsonObj);
    console.log('Matches won per year');
    console.log(noOfMatchesWon);
    csv().fromFile('../data/deliveries.csv').then((jsonObj) => {
        let deliveries2016 = ipldata.deliveriesForYear(jsonObj, matchId2016);
        let deliveries2015 = ipldata.deliveriesForYear(jsonObj, matchId2015);
        let extraRunsForYear = ipldata.getExtraRunsPerTeamForYear(deliveries2016);
        console.log('Extra run per year');
        console.log(extraRunsForYear);
        let economicalBowlerForYear = ipldata.getEconomicalBowlersForYear(deliveries2015);
        console.log('EconomyBowler per year');
        console.log(economicalBowlerForYear);
        dumpIntoJson(noOfMatchesPlayed, 'noOfMatchesPlayed');
        dumpIntoJson(noOfMatchesWon, 'noOfMatchesWon');
        dumpIntoJson(extraRunsForYear, 'extraRunsForYear');
        dumpIntoJson(economicalBowlerForYear, 'economicalBowlerForYear');
    })
});
function dumpIntoJson(jsonObject, jsonObjectName){
    fs.writeFile(`../public/json/${jsonObjectName}.json`,JSON.stringify(jsonObject), (err) => {
        if(err) {
            console.error(err);
            return;
        };
        console.log("File has been created");
    });
}
