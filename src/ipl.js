// const loki = require('../data')
var csvToJson=require('./csvtojson')
var matchesData = csvToJson('../data/matches.csv')
var deliveryData = csvToJson('../data/deliveries.csv')

const getNoOfMatchesPlayed = (matches) => {
  return matches.reduce((noOfMatches, match) => {
    if(!noOfMatches[match['season']]){
        noOfMatches[match['season']] = 1;
      }
    else {
        noOfMatches[match['season']] += 1;
      }
    return noOfMatches;
  },{});
}
const getNoOfMatchesWonPerTeamPerYear = (matches) => {
  return matches.reduce((noOfMatchesWon, match) => {
    if(!noOfMatchesWon[match['winner']]){
      noOfMatchesWon[match['winner']] = {};
    }
    else if(!noOfMatchesWon[match['winner']][match['season']]){
      noOfMatchesWon[match['winner']][match['season']] = 1;
    }
    else {
      noOfMatchesWon[match['winner']][match['season']] += 1;
    }
    return noOfMatchesWon;
  },{})
};
const getExtraRunsPerTeamForYear = (deliveries) => {
 return deliveries.reduce((extraRuns, delivery) => {
   if(!extraRuns[delivery['bowling_team']]){
     extraRuns[delivery['bowling_team']] = Number(delivery['extra_runs']);
   }
   else{
     extraRuns[delivery['bowling_team']] += Number(delivery['extra_runs']);
   }
   return extraRuns;
 },{});
}
const getEconomicalBowlersForYear = (deliveries) => {
 const bowlerEconomy = deliveries.reduce((bowlerEconomy, delivery) => {
      if(!bowlerEconomy[delivery['bowler']]){
        bowlerEconomy[delivery['bowler']] = {};
      }
      if(!bowlerEconomy[delivery['bowler']]['runs']){
        bowlerEconomy[delivery['bowler']]['balls'] = 1;
        bowlerEconomy[delivery['bowler']]['runs'] = Number(delivery['wide_runs']) + Number(delivery['noball_runs']) + Number(delivery['batsman_runs']);
      }
      else{
        bowlerEconomy[delivery['bowler']]['balls'] += 1;
        bowlerEconomy[delivery['bowler']]['runs'] += Number(delivery['wide_runs']) + Number(delivery['noball_runs']) + Number(delivery['batsman_runs']);
      }
      return bowlerEconomy;
 },{})
 const bowlerEconom = Object.keys(bowlerEconomy)
 return bowlerEconom.reduce((economy, bowler) => {
   economy[bowler] = (bowlerEconomy[bowler]['runs'] / bowlerEconomy[bowler]['balls']) * 6
   return economy;
 },{})

}

const matchIdForYear = (matches, year) => {
  return matches.filter((match) =>match['season'] == year).map((match) => match['id']);
}
const deliveriesForYear = (deliveries, matchId) => {
  return deliveries.filter((delivery) => matchId.indexOf(delivery['match_id']) !== -1);
}

module.exports.getNoOfMatchesPlayed = getNoOfMatchesPlayed;
module.exports.getNoOfMatchesWonPerTeamPerYear = getNoOfMatchesWonPerTeamPerYear;
module.exports.getExtraRunsPerTeamForYear = getExtraRunsPerTeamForYear;
module.exports.getEconomicalBowlersForYear = getEconomicalBowlersForYear;
module.exports.matchIdForYear = matchIdForYear;
module.exports.deliveriesForYear = deliveriesForYear;