function getJsonData(jsonName) {
    fetch(`./json/${jsonName}.json`)
      .then(response => response.json())
      .then(data => {
        if (jsonName === 'noOfMatchesPlayed')
          highChartForMatchesPlayed(data, jsonName);
        else if (jsonName === 'noOfMatchesWon')
          highChartForMatchesWon(data, jsonName);
        else if (jsonName === 'extraRunsForYear')
          highChartForExtraRunsForYear(data, jsonName);
        else if (jsonName === 'economicalBowlerForYear')
          highChartForeconomicalBowlerForYear(data, jsonName);
      })
      .catch((err) => {
        console.log('Fetch Error :-S', err);
      });
  }
  
function highChartForMatchesPlayed(noOfMatchesPlayed){
    Highcharts.chart('container1', {
        chart: {
          type: 'column'
        },
        title: {
          text: 'No of Matches Played Per Year'
        },
        xAxis: {
          categories: Object.keys(noOfMatchesPlayed)
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Matches Played'
          }
        },
        plotOptions: {
          column: {
            pointPadding: 0.2,
            borderWidth: 0
          }
        },
        series: [{
          name: name,
          colorByPoint: true,
          data: Object.values(noOfMatchesPlayed),
        }]
      })
}
function highChartForMatchesWon(noOfMatchesWon){
  let years = ['2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017']
  Highcharts.chart('container2', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'No of Matches Played Per Year'
    },
    xAxis: {
      categories: Object.keys(noOfMatchesWon)
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Matches Won'
      }
    },
    plotOptions: {
      column: {
        stacking: 'normal',
      }
    },
    series: years.reduce((array, year) => {
          array.push({
            name: year,
            type: 'column',
            data: Object.keys(noOfMatchesWon).reduce((array, team) => {
              if (noOfMatchesWon[team][year] == undefined) {
                array.push(0);
              } else {
                array.push(noOfMatchesWon[team][year])
              }
              console.log(array);
              return array;
          }, [])  
        })
        return array
      }, [])
    })

}

function highChartForExtraRunsForYear(extraRunsForYear){
    Highcharts.chart('container3', {
        chart: {
          type: 'column'
        },
        title: {
          text: 'Extra Runs For Year'
        },
        xAxis: {
          categories: Object.keys(extraRunsForYear)
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Extra Runs'
          }
        },
        plotOptions: {
          column: {
            pointPadding: 0.2,
            borderWidth: 0
          }
        },
        series: [{
          name: name,
          colorByPoint: true,
          data: Object.values(extraRunsForYear),
        }]
      }) 
}

function highChartForeconomicalBowlerForYear(economicalBowlerForYear){
    Highcharts.chart('container4', {
        chart: {
          type: 'column'
        },
        title: {
          text: 'Economical Bowler Per Year'
        },
        xAxis: {
          categories: Object.keys(economicalBowlerForYear)
        },
        yAxis: {
          min: 0,
          title: {
            text: `Bowler's Economy`
          }
        },
        plotOptions: {
          column: {
            pointPadding: 0.2,
            borderWidth: 0
          }
        },
        series: [{
          name: name,
          colorByPoint: true,
          data: Object.values(economicalBowlerForYear),
        }]
      }) 
}

function formatdataForColumnChart(object) {
  //write your code to convert jsondata for the column chart format.
}

function formatdataForBarChart(jsondata) {
   
}

getJsonData('noOfMatchesPlayed');
getJsonData('noOfMatchesWon');
getJsonData('extraRunsForYear'); 
getJsonData('economicalBowlerForYear');

